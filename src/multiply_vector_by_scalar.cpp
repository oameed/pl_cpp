///////////////////////////////////////////
/// FINITE DIFFERENCE METHOD (FDM)      ///
/// UTILITIES FUNCTION DEFINITIONS      ///
/// VECTOR MULTIPLICATION WITH CONSTANT ///
/// by: OAMEED NOAKOASTEEN              ///
///////////////////////////////////////////

using namespace std                                     ;

void multiply_vector_by_scalar(const double   &C   ,
                               const int      &SIZE,
                                     double**  X    ) {
 for (int i=0; i<SIZE; i++)
  *(*X+i)=(*(*X+i))*C                                   ;
}


