/////////////////////////////////////////////
/// FINITE DIFFERENCE METHOD (FDM)        ///
/// MATRIX A, VECTOR G                    ///
/// HEADER FILE                           ///
/// by: OAMEED NOAKOASTEEN                ///
/////////////////////////////////////////////


#ifndef MAVGD_H
#define MAVGD_H

void get_matrix_A(const vector<vector<int>> &NGBR,
                  const int                 &SIZE,
                        double**             A    );

void get_vector_G(const int                                  &SIZE  ,
                  const vector<int>                          &BND   ,
                  const vector<vector<tuple<double,double>>> &BNDEXT,
                  const double                               &DX    ,
                  const double                               &V     ,
                        double**                              G      );

#endif


