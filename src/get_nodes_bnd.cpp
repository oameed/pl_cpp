/////////////////////////////////////////////
/// FINITE DIFFERENCE METHOD (FDM)        ///
/// GEOMETRIC DOMAIN FUNCTION DEFINITIONS ///
/// by: OAMEED NOAKOASTEEN                ///
/////////////////////////////////////////////

#include <vector>
#include <tuple>
#include <math.h>
#include <algorithm>

using namespace std;

void get_index_of_tuple_in_vector_of_tuples(const vector<tuple<double,double>> &X  ,
                                            const vector<tuple<double,double>> &Y  ,
                                                  vector<int>                  &IDX ) {
 for (int i=0; i<Y.size(); i++) {
  auto iter=find_if(X.begin(),X.end(),[&Y, &i](const tuple<double,double> &e){return get<0>(e)==get<0>(Y[i]) &&
                                                                                     get<1>(e)==get<1>(Y[i])   ;});
  IDX.push_back(distance(X.begin(),iter))                                                                         ;
 }
}

void get_index_of_int_in_vector_of_ints(const vector<int> &X  ,
                                        const int         &Y  ,
                                              int         &IDX ){
 auto iter=find_if(X.begin(),X.end(),[&Y](const int &e){return e==Y;});
 IDX      =distance(X.begin(),iter)                                   ;
}

vector<int> Range(const int &RB,
                  const int &RE ) {
 vector<int> vec          ;
 for (int i=RB; i<RE; i++)
  vec.push_back(i)        ;
 return vec               ;
}


double Round(const double &X,
             const int    &P ){
 double value=round(X*pow(10,P))/pow(10,P); 
 return value                             ;
}

vector<int> concatenate_vector_of_vectors(const vector<vector<int>> &X) {
 vector<int> v                                ;
 v.insert(v.begin(), X[0].begin(), X[0].end());
 for (int i=1; i<X.size(); i++)
  v.insert(v.end(), X[i].begin(), X[i].end()) ;
 return v                                     ;
}

vector<int> get_sorting_index(const vector<int> &X) {
 vector<tuple<int,int>> v        ;
 vector<int>            vec      ;
 for (int i=0; i<X.size(); i++)
  v.push_back(make_tuple(X[i],i));
 sort(v.begin(),v.end())         ;
 for (int i=0; i<v.size(); i++)
  vec.push_back(get<1>(v[i]))    ;
 return vec                      ;
}


template <class T>
T sort_with_index(const T           &X    ,
                  const vector<int> &INDEX ) {
 T v                               ;
 for (int i=0; i<INDEX.size(); i++) 
  v.push_back(X[INDEX[i]])         ;
 return v                          ;
}

void get_1d_boundary_index(const vector<int>                  &J    , 
                           const vector<int>                  &I    ,  
                           const vector<double>               &X    , 
                           const vector<double>               &Y    ,     
                           const vector<tuple<double,double>> &NODES,
                                 vector<vector<int>>          &LIST  ) {
 vector<tuple<double,double>> BND                         ;
 vector<int>                  indeces                     ;

 for  (int j=0; j<J.size(); j++) {
  for (int i=0; i<I.size(); i++) {
   BND.push_back(make_tuple(X[I[i]],Y[J[j]]))             ;
  }
 }
 get_index_of_tuple_in_vector_of_tuples(NODES,BND,indeces);
 LIST.push_back(indeces)                                  ;
}

void get_0d_boundary_index(const vector<tuple<double,double>> &COORD,
                           const vector<tuple<double,double>> &NODES,
                                 vector<vector<int>>          &LIST  ) {
 vector<int>                  indeces                       ;
 get_index_of_tuple_in_vector_of_tuples(NODES,COORD,indeces);
 LIST.push_back(indeces)                                    ;
}

void get_nodes_bnd(const double                               &XEND   ,  
                   const double                               &YEND   , 
                   const int                                  &N      ,
                         vector<tuple<double,double>>         &nodes  ,
                         vector<vector<int>>                  &ngbr   ,
                         vector<int>                          &BOUND  ,
                         vector<vector<tuple<double,double>>> &bndngbr,
                         double                               &delta  ,
                         int                                  &Ny      ) {

 int                                  digits=5                                                     ;
 int                                  INDEX                                                        ;
 vector<double>                       x,y                                                          ;
 vector<vector<int>>                  bound,
                                      TEMP                                                         ;
 vector<vector<int>>                  neighbors                                                    ; //   LIST OF INTERIOR NEIGHBORS       FOR BOUNDARY NODES
 vector<vector<int>>                  NEIGHBORS                                                    ; //   LIST OF          NEIGHBORS       FOR INTERIOR NODES
 tuple<double,double>                 node              , 
                                      node_to_the_right , 
                                      node_to_the_top   , 
                                      node_to_the_left  , 
                                      node_to_the_bottom                                           ;
 vector<int>                          index             , 
                                      indexsorted       , 
                                      indexinside                                                  ;
 
 
 delta =(XEND-0)/N                                                                                 ;
 Ny    =floor((YEND-0)/delta)                                                                      ;
 
 for (int i=0; i<N +1; i++)
  x.push_back(Round(i*delta,digits))                                                               ;
 for (int i=0; i<Ny+1; i++) 
  y.push_back(Round(i*delta,digits))                                                               ;
 
 for  (int j=0; j<y.size(); j++) {
  for (int i=0; i<x.size(); i++) {
   nodes.push_back(make_tuple(x[i],y[j]))                                                          ;
  }
 }
 
 get_1d_boundary_index(Range(0 ,1   ),Range(1,N  ),x,y,                              nodes,bound)  ; // GET NODES ON 1D BOUNDARY
 get_1d_boundary_index(Range(1 ,Ny  ),Range(N,N+1),x,y,                              nodes,bound)  ;
 get_1d_boundary_index(Range(Ny,Ny+1),Range(1,N  ),x,y,                              nodes,bound)  ;
 get_1d_boundary_index(Range(1 ,Ny  ),Range(0,1  ),x,y,                              nodes,bound)  ;
 get_0d_boundary_index(vector<tuple<double,double>> {make_tuple(0.0     ,0.0     )}, nodes,bound)  ; // GET NODES ON 0D BOUNDARY
 get_0d_boundary_index(vector<tuple<double,double>> {make_tuple(x.back(),0.0     )}, nodes,bound)  ;
 get_0d_boundary_index(vector<tuple<double,double>> {make_tuple(x.back(),y.back())}, nodes,bound)  ;
 get_0d_boundary_index(vector<tuple<double,double>> {make_tuple(0.0     ,y.back())}, nodes,bound)  ;
 
 for (int i=0; i<bound[0].size(); i++) {                                                             // GET INTERIOR NEIGHBORS FOR 1D BOUNDARY NODES 
  node              =nodes[bound[0][i]]                                                            ;
  node_to_the_right =make_tuple(Round(get<0>(node)+delta,digits),      get<1>(node)              ) ;
  node_to_the_top   =make_tuple(      get<0>(node)              ,Round(get<1>(node)+delta,digits)) ;
  node_to_the_left  =make_tuple(Round(get<0>(node)-delta,digits),      get<1>(node)              ) ;
  get_index_of_tuple_in_vector_of_tuples(nodes                                            ,
                                         vector<tuple<double,double>> {node_to_the_right,
                                                                       node_to_the_top  ,
                                                                       node_to_the_left  }, 
                                         index                                             )       ;
  neighbors.push_back(index)                                                                       ;
  index.clear()                                                                                    ;
  node_to_the_bottom=make_tuple(      get<0>(node)              ,Round(get<1>(node)-delta,digits)) ; // GET EXTERIOR NEIGHBOR COORDS FOR 1D BOUNDARY NODES
  bndngbr.push_back(vector<tuple<double,double>> {node_to_the_bottom})                             ;
 }
 
 for (int i=0; i<bound[1].size(); i++) {                                                             
  node              =nodes[bound[1][i]]                                                            ;
  node_to_the_top   =make_tuple(      get<0>(node)              ,Round(get<1>(node)+delta,digits)) ;  
  node_to_the_left  =make_tuple(Round(get<0>(node)-delta,digits),      get<1>(node)              ) ;
  node_to_the_bottom=make_tuple(      get<0>(node)              ,Round(get<1>(node)-delta,digits)) ; 
  get_index_of_tuple_in_vector_of_tuples(nodes                                             ,
                                         vector<tuple<double,double>> {node_to_the_top   ,
                                                                       node_to_the_left  ,
                                                                       node_to_the_bottom }, 
                                         index                                              )      ;
  neighbors.push_back(index)                                                                       ;
  index.clear()                                                                                    ;  
  node_to_the_right =make_tuple(Round(get<0>(node)+delta,digits),      get<1>(node)              ) ;
  bndngbr.push_back(vector<tuple<double,double>> {node_to_the_right})                              ;
 }
 
 for (int i=0; i<bound[2].size(); i++) {                                                             
  node              =nodes[bound[2][i]]                                                            ;
  node_to_the_bottom=make_tuple(      get<0>(node)              ,Round(get<1>(node)-delta,digits)) ; 
  node_to_the_right =make_tuple(Round(get<0>(node)+delta,digits),      get<1>(node)              ) ;
  node_to_the_left  =make_tuple(Round(get<0>(node)-delta,digits),      get<1>(node)              ) ;
  get_index_of_tuple_in_vector_of_tuples(nodes                                             ,
                                         vector<tuple<double,double>> {node_to_the_bottom,
                                                                       node_to_the_right ,
                                                                       node_to_the_left   }, 
                                         index                                              )      ;
  neighbors.push_back(index)                                                                       ;
  index.clear()                                                                                    ;  
  node_to_the_top   =make_tuple(      get<0>(node)              ,Round(get<1>(node)+delta,digits)) ;  
  bndngbr.push_back(vector<tuple<double,double>> {node_to_the_top})                                ; 
 }
 
 for (int i=0; i<bound[3].size(); i++) {                                                             
  node              =nodes[bound[3][i]]                                                            ;
  node_to_the_bottom=make_tuple(      get<0>(node)              ,Round(get<1>(node)-delta,digits)) ; 
  node_to_the_right =make_tuple(Round(get<0>(node)+delta,digits),      get<1>(node)              ) ;
  node_to_the_top   =make_tuple(      get<0>(node)              ,Round(get<1>(node)+delta,digits)) ;  
  get_index_of_tuple_in_vector_of_tuples(nodes                                             ,
                                         vector<tuple<double,double>> {node_to_the_bottom,
                                                                       node_to_the_right ,
                                                                       node_to_the_top    }, 
                                         index                                              )      ;
  neighbors.push_back(index)                                                                       ;
  index.clear()                                                                                    ;  
  node_to_the_left  =make_tuple(Round(get<0>(node)-delta,digits),      get<1>(node)              ) ;
  bndngbr.push_back(vector<tuple<double,double>> {node_to_the_left})                               ; 
 }
 
 node              =nodes[bound[4][0]]                                                             ; // GET INTERIOR NEIGHBORS FOR 0D BOUNDARY NODES
 node_to_the_right =make_tuple(Round(get<0>(node)+delta,digits),      get<1>(node)              )  ;
 node_to_the_top   =make_tuple(      get<0>(node)              ,Round(get<1>(node)+delta,digits))  ;
 get_index_of_tuple_in_vector_of_tuples(nodes                                             ,
                                        vector<tuple<double,double>> {node_to_the_right ,
                                                                      node_to_the_top    }, 
                                        index                                             )        ;
 neighbors.push_back(index)                                                                        ;
 index.clear()                                                                                     ;
 node_to_the_left  =make_tuple(Round(get<0>(node)-delta,digits),      get<1>(node)              )  ; // GET EXTERIOR NEIGHBOR COORDS FOR 0D BOUNDARY NODES
 node_to_the_bottom=make_tuple(      get<0>(node)              ,Round(get<1>(node)-delta,digits))  ; 
 bndngbr.push_back(vector<tuple<double,double>> {node_to_the_left  ,
                                                 node_to_the_bottom })                             ; 
 
 node              =nodes[bound[5][0]]                                                             ; 
 node_to_the_top   =make_tuple(      get<0>(node)              ,Round(get<1>(node)+delta,digits))  ;
 node_to_the_left  =make_tuple(Round(get<0>(node)-delta,digits),      get<1>(node)              )  ; 
 get_index_of_tuple_in_vector_of_tuples(nodes                                           ,
                                        vector<tuple<double,double>> {node_to_the_top ,
                                                                      node_to_the_left }, 
                                        index                                             )        ;
 neighbors.push_back(index)                                                                        ;
 index.clear()                                                                                     ;
 node_to_the_right =make_tuple(Round(get<0>(node)+delta,digits),      get<1>(node)              )  ;
 node_to_the_bottom=make_tuple(      get<0>(node)              ,Round(get<1>(node)-delta,digits))  ; 
 bndngbr.push_back(vector<tuple<double,double>> {node_to_the_right ,
                                                 node_to_the_bottom })                             ; 
 
 node              =nodes[bound[6][0]]                                                             ; 
 node_to_the_bottom=make_tuple(      get<0>(node)              ,Round(get<1>(node)-delta,digits))  ; 
 node_to_the_left  =make_tuple(Round(get<0>(node)-delta,digits),      get<1>(node)              )  ; 
 get_index_of_tuple_in_vector_of_tuples(nodes                                           ,
                                        vector<tuple<double,double>> {node_to_the_bottom,
                                                                      node_to_the_left   }, 
                                        index                                              )       ;
 neighbors.push_back(index)                                                                        ;
 index.clear()                                                                                     ;
 node_to_the_right =make_tuple(Round(get<0>(node)+delta,digits),      get<1>(node)              )  ;
 node_to_the_top   =make_tuple(      get<0>(node)              ,Round(get<1>(node)+delta,digits))  ;
 bndngbr.push_back(vector<tuple<double,double>> {node_to_the_right,
                                                 node_to_the_top   })                              ; 
 
 node              =nodes[bound[7][0]]                                                             ; 
 node_to_the_bottom=make_tuple(      get<0>(node)              ,Round(get<1>(node)-delta,digits))  ; 
 node_to_the_right =make_tuple(Round(get<0>(node)+delta,digits),      get<1>(node)              )  ;
 get_index_of_tuple_in_vector_of_tuples(nodes                                           ,
                                        vector<tuple<double,double>> {node_to_the_bottom,
                                                                      node_to_the_right  }, 
                                        index                                              )       ;
 neighbors.push_back(index)                                                                        ;
 index.clear()                                                                                     ;
 node_to_the_top   =make_tuple(      get<0>(node)              ,Round(get<1>(node)+delta,digits))  ;
 node_to_the_left  =make_tuple(Round(get<0>(node)-delta,digits),      get<1>(node)              )  ; 
 bndngbr.push_back(vector<tuple<double,double>> {node_to_the_top ,
                                                 node_to_the_left })                               ; 
 
 BOUND      =concatenate_vector_of_vectors(bound)                                                  ; // SORT BOUNDARY POINTS ACCORDING TO THEIR NUMBER
 indexsorted=get_sorting_index(BOUND)                                                              ;
 BOUND      =sort_with_index<vector<int>>(                         BOUND    ,indexsorted)          ;
 neighbors  =sort_with_index<vector<vector<int>>>(                 neighbors,indexsorted)          ; // SORT INTERIOR NEIGHBOR INFORMATION ACCORDINGLY
 bndngbr    =sort_with_index<vector<vector<tuple<double,double>>>>(bndngbr  ,indexsorted)          ; // SORT EXTERIOR NEIGHBOR INFORMATION ACCORDINGLY
 
 for (int i=0; i<nodes.size(); i++) {                                                                // GET  NEIGHBORS FOR INTERIOR NODES
  if (not (find(BOUND.begin(),BOUND.end(),i) != BOUND.end())) {
   indexinside.push_back(i)                                                                        ;
  }
 }
 
 for (int i=0; i<indexinside.size(); i++) {
  node              =nodes[indexinside[i]]                                                         ;
  node_to_the_bottom=make_tuple(      get<0>(node)              ,Round(get<1>(node)-delta,digits)) ; 
  node_to_the_right =make_tuple(Round(get<0>(node)+delta,digits),      get<1>(node)              ) ;
  node_to_the_top   =make_tuple(      get<0>(node)              ,Round(get<1>(node)+delta,digits)) ;
  node_to_the_left  =make_tuple(Round(get<0>(node)-delta,digits),      get<1>(node)              ) ;
  get_index_of_tuple_in_vector_of_tuples(nodes                                             ,
                                         vector<tuple<double,double>> {node_to_the_bottom,
                                                                       node_to_the_right ,
                                                                       node_to_the_top   ,
                                                                       node_to_the_left   }, 
                                         index                                              )      ;
  NEIGHBORS.push_back(index)                                                                       ;
  index.clear()                                                                                    ; 
 }
 
 for (int i=0; i<nodes.size(); i++) {                                                                // GENERATE AN ORDERED LIST OF NEIGHBORS FOR ALL GRID POINTS
  if (find(BOUND.begin()       , BOUND.end()      , i) != BOUND.end()) {
   get_index_of_int_in_vector_of_ints(BOUND       ,i,INDEX);
   TEMP =neighbors                                         ;
  }
  else {
   if (find(indexinside.begin(), indexinside.end(), i) !=indexinside.end()) {
    get_index_of_int_in_vector_of_ints(indexinside,i,INDEX);
    TEMP=NEIGHBORS                                         ;
   }
  }
  ngbr.push_back(TEMP[INDEX])                              ;
 }

}


