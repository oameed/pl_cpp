//////////////////////////////////////
/// FINITE DIFFERENCE METHOD (FDM) ///
/// GENERATE VECTOR G              ///
/// by: OAMEED NOAKOASTEEN         ///
//////////////////////////////////////

#include <vector>
#include <tuple>

using namespace std;

void get_vector_G(const int                                  &SIZE  ,
                  const vector<int>                          &BND   ,
                  const vector<vector<tuple<double,double>>> &BNDEXT,
                  const double                               &DX    ,
                  const double                               &V     ,
                        double**                              G      ) {
 
 int flag                                                                 ;
 
 *G        =new double[SIZE]                                              ; // INITIALIZE VECTOR G
 for (int i=0; i<      SIZE; i++)
  *(*G+i)  =0;                                                            ;
 
 for  (int i=0; i<BND.size()      ; i++) {
  flag=0                                                                  ;
  for (int j=0; j<BNDEXT[i].size(); j++) {
   if (get<0>(BNDEXT[i][j])<0) {
    flag=1                                                                ;
    break                                                                 ;
   }
  }
  if (flag==1) {
   for (int j=0; j<BNDEXT[i].size(); j++)
    *(*G+BND[i])=*(*G+BND[i])+1*V                                         ;
  }
 }
}


