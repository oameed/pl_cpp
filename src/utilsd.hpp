/////////////////////////////////////////////
/// FINITE DIFFERENCE METHOD (FDM)        ///
/// UTILITIES FUNCTIONS                   ///
/// HEADER FILE                           ///
/// by: OAMEED NOAKOASTEEN                ///
/////////////////////////////////////////////


#ifndef UTILSD_H
#define UTILSD_H

void multiply_vector_by_scalar(const double   &C   ,
                               const int      &SIZE,
                                     double**  X    );

void exportArray(const vector<string> &PATH,
                 const int            &Nx  ,
                 const int            &Ny  ,
                       double**        X    );


#endif


