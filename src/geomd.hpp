/////////////////////////////////////////////
/// FINITE DIFFERENCE METHOD (FDM)        ///
/// GEOMETRIC DOMAIN FUNCTION DEFINITIONS ///
/// HEADER FILE                           ///
/// by: OAMEED NOAKOASTEEN                ///
/////////////////////////////////////////////


#ifndef GEOMD_H
#define GEOMD_H

void get_nodes_bnd(const double                               &XEND   ,  
                   const double                               &YEND   , 
                   const int                                  &N      ,
                         vector<tuple<double,double>>         &nodes  ,
                         vector<vector<int>>                  &ngbr   ,
                         vector<int>                          &BOUND  ,
                         vector<vector<tuple<double,double>>> &bndngbr,
                         double                               &delta  ,
                         int                                  &Ny      );

#endif


