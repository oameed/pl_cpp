//////////////////////////////////////
/// FINITE DIFFERENCE METHOD (FDM) ///
/// SOLVER                         ///
/// by: OAMEED NOAKOASTEEN         ///
//////////////////////////////////////

#include <iostream>
#include <vector>
#include <tuple>

using namespace std                                             ;

#include "geomd.hpp"
#include "mAvGd.hpp"
#include "utilsd.hpp"
#include "lapacke.h"

int main(int    argc, 
         char** argv ) {
 
 vector<tuple<double,double>>         nodes                     ; //   LIST OF ALL NODES OF THE GRID
 vector<vector<int>>                  ngbr                      ; //   LIST OF          NEIGHBORS       FOR ALL GRID POINTS
 vector<int>                          bnd                       ; //   LIST OF ALL NODES OF THE GRID THAT ARE ON THE BOUNDARIES
 vector<vector<tuple<double,double>>> bndext                    ; //   LIST OF EXTERIOR NEIGHBOR COORDS FOR BOUNDARY NODES
 double                               dx                        ;
 int                                  Ny                        ;
 
 int                                  SIZE                      ;
 double*                              A[1]                      ; //   THE CONNECTIVITY MATRIX
 double*                              G[1]                      ; //   VECTOR OF BOUNDARY VALUES
 
 vector<string>                       PATH = {".."         ,
                                              "simulations", 
                                              argv[1]       }   ;
 
 get_nodes_bnd(stod(argv[2]),
               stod(argv[3]),
               stoi(argv[4]),
               nodes        ,
               ngbr         ,
               bnd          ,
               bndext       ,
               dx           ,
               Ny            )                                  ;
 
 SIZE=nodes.size()                                              ;
 get_matrix_A(ngbr,
              SIZE,
              A    )                                            ;
 get_vector_G(SIZE         ,
              bnd          ,
              bndext       ,
              dx           ,
              stod(argv[5]),
              G             )                                   ;
 
 multiply_vector_by_scalar(-1  ,
                           SIZE,
                           G    )                               ;

///////////////////////////
/// SOLVE LINEAR SYSTEM ///
///////////////////////////

 int  M    =SIZE                                                ; // dgetrf         ; The number of rows    of the matrix A
 int  N    =SIZE                                                ; // dgetrf & dgetrs; The number of columns of the matrix A
 int  LDA  =M                                                   ; // dgetrf & dgetrs; The leading dimension of the array A:  LDA >= max(1,M)
 int* IPIV =new int [SIZE]                                      ; // dgetrf & dgetrs; 
 int  INFO =0                                                   ; // dgetrf & dgetrs; 
 
 char TRANS='N'                                                 ; // dgetrs         ; Specifies the form of the system of equations
 int  NRHS =1                                                   ; // dgetrs         ; The number of right hand sides, i.e., the number of columns of the matrix B
 int  LDB  =SIZE                                                ; // dgetrs         ; The leading dimension of the array B.  LDB >= max(1,N)

 cout << " BEGIN LU FACTORIZATION " << endl                     ;
 LAPACK_dgetrf(&M,&N,*A,&LDA,IPIV,&INFO)                        ;
 if (INFO) {
  cout << " AN ERROR OCCURED : " << INFO << endl                ;
 }
 else {
  cout << " SOLVING THE SYSTEM ... "     << endl                ;
  dgetrs_(&TRANS,&N,&NRHS,*A,&LDA,IPIV,*G,&LDB,&INFO)           ;
  if (INFO) {
   cout << " AN ERROR OCCURED : " << INFO << endl               ;
  }
  else {
   cout << " SOLVE SUCCESSFUL  "  << endl                       ; // THE RESULTS (OUTPUTS) ARE PLACED IN THE G VECTOR
  } 
 }

///////////////////////////
 
 exportArray(PATH         ,
             stoi(argv[4]),
             Ny           ,
             G             )                                    ;
 
 cout << "FINISHED !" << endl                                   ;

 return 0                                                       ;
}

// argv[1]: SPECIFY SIMULATION VERSION
// argv[2]: LENGTH  OF DOMAIN IN X DIRECTION
// argv[3]: LENGTH  OF DOMAIN IN Y DIRECTION
// argv[4]: NUMBER  OF DIVISIONS ALONG X-AXIS
// argv[5]: BOUNDARY VOLTAGE AMPLITUDE


