///////////////////////////////////////////
/// FINITE DIFFERENCE METHOD (FDM)      ///
/// UTILITIES FUNCTION DEFINITIONS      ///
/// EXPORT ARRAYS                       ///
/// by: OAMEED NOAKOASTEEN              ///
///////////////////////////////////////////

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <iomanip>

using namespace std                              ;

void exportArray(const vector<string> &PATH,
                 const int            &Nx  ,
                 const int            &Ny  ,
                       double**        X    ) {
 
 string filename=PATH[0]+string("/")             ;
 int    COL     =Nx+1                            ;
 int    ROW     =Ny+1                            ;
 
 for(int i=1; i<PATH.size(); i++)
  filename=filename+PATH[i]+string("/")          ;
 filename =filename+string("data")+string(".csv");
 
 ofstream fobj                                   ;
 fobj.open(filename)                             ;
 for  (int i=0; i<ROW; i++) {
  for (int j=0; j<COL; j++) {
   fobj << setw(6) << *(*X+(i*COL+j)) << ","     ;
  }
  fobj << endl                                   ;
 }
 
}


