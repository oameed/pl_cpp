//////////////////////////////////////
/// FINITE DIFFERENCE METHOD (FDM) ///
/// GENERATE MATRIX A              ///
/// by: OAMEED NOAKOASTEEN         ///
//////////////////////////////////////

#include <vector>
#include <tuple>

using namespace std;

void get_matrix_A(const vector<vector<int>> &NGBR,
                  const int                 &SIZE,
                        double**             A    ){

 *A        =new double[SIZE*SIZE]                     ; // INITIALIZE MATRIX A
 for (int i=0; i<      SIZE*SIZE ; i++)
  *(*A+i)  =0                                         ;
 
 for  (int i=0; i<SIZE          ; i++) {
  for (int j=0; j<NGBR[i].size(); j++) {
   *(*A+(i*SIZE+NGBR[i][j]))= 1                       ;
  }
  *( *A+(i*SIZE+i         ))=-4                       ;
 }

}


