# Programming Language: C++

This project is **C++** implementation of the following problem:
* Finite Difference Method (FDM) applied to distribution of the electrostatic voltage inside a source-less rectangular region [[View Repository on GitLab]](https://gitlab.com/oameed/cem_fdm_2d)

## References

[[1].](https://www.oreilly.com/library/view/managing-projects-with/0596006101/) 2004. Mecklenburg. _Managing Projects with GNU Make. 3rd Edition_ [[view online]](https://www.oreilly.com/openbook/make3/book/index.csp)  

_Useful Web Tutorials_

[[2].](https://www.geeksforgeeks.org/cpp-tutorial/) 0000. GeeksforGeeks C++ Tutorial  
[[3].](https://www.w3schools.com/cpp/) 0000. W3Schools C++ Tutorial  
[[4].](https://www.programiz.com/cpp-programming) 0000. Programiz C++ Tutorial  
[[5].](https://www.learncpp.com/) 0000. Learn cpp  
[[6].](https://www.tutorialspoint.com/cplusplus/index.htm) 0000. tutorialspoint C++ Tutorial   
[[7].](https://dynamithead.wordpress.com/2012/06/30/introduction-to-how-to-call-lapack-from-a-cc-program-example-solving-a-system-of-linear-equations/) 2012. Introduction to how to call LAPACK from a C/C++ program

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.74  T=0.02 s (961.7 files/s, 52189.2 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
C++                              6             86             41            423
Markdown                         1             14              0             42
Bourne Shell                     3             22              0             41
C/C++ Header                     3             21             18             34
make                             1             12              5             27
MATLAB                           1              6              5             17
-------------------------------------------------------------------------------
SUM:                            15            161             69            584
-------------------------------------------------------------------------------
</pre>


## How to Run
* This project uses [LAPACK](http://www.netlib.org/lapack/) for Linear Algebra subroutines.
* To run:
  1. Download LAPACK's zipped library and place it in `lib/`
  2. Build LAPACK from source:  
     `cd` to the main project directory  
     `./run/sh/build_LAPACK.sh`  
  3. Build project executable from source:  
     `cd` to the main project directory  
     `./run/sh/build_executable.sh`  
  4. Run simulation:  
     `cd` to the main project directory  
     `./run/sh/run_v01.sh`

## Simulation 'v01'

|     |
|:---:|
![][fig1]

[fig1]:simulations/v01/data.png




