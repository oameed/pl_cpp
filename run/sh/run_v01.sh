#! /bin/bash

cd bin

echo ' CREATING PROJECT DIRECTORY '                      'v01'
rm     -rf                                 ../simulations/v01
tar    -xzf  ../simulations/v00.tar.gz -C  ../simulations
mv           ../simulations/v00            ../simulations/v01

echo ' RUNNING FINITE DIFFERENCE SIMULATION'
./solver.exe v01 1 0.5 50 1

cd ../run/fdm

echo ' RUNNING VISUALIZATIONS '
matlab -nodisplay -nosplash -nodesktop -r "graphics('v01');exit;"


