#! /bin/bash

echo " BUILDING LAPACK"

cd           lib
TARNAME=(  lapack*.tar.gz) 
DIRNAME=${TARNAME%.tar.gz}

rm     -rf   ../include/lapack
rm     -rf   $DIRNAME
rm     -rf   lapack

tar    -xzvf $TARNAME
cd           $DIRNAME

cp           make.inc.example make.inc
ulimit -s    unlimited
make   -j    8

mkdir                     ../lapack
cp           liblapack.a  ../lapack/liblapack.a
cp           librefblas.a ../lapack/librefblas.a
cp           libtmglib.a  ../lapack/libtmglib.a

mkdir                                           ../../include/lapack
cp           LAPACKE/include/lapacke.h          ../../include/lapack/lapacke.h
cp           LAPACKE/include/lapack.h           ../../include/lapack/lapack.h
cp           LAPACKE/include/lapacke_config.h   ../../include/lapack/lapacke_config.h
cp           LAPACKE/include/lapacke_mangling.h ../../include/lapack/lapacke_mangling.h
cp           LAPACKE/include/lapacke_utils.h    ../../include/lapack/lapacke_utils.h

cd           ../
rm     -rf   $DIRNAME


