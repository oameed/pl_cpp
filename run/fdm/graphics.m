%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FINITE DIFFERENCE METHOD (FDM) %%%
%%% GRAPHICS                       %%%
%%% by: OAMEED NOAKOASTEEN         %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function graphics(V)
close all
clc

PATH=fullfile('..','..','simulations',V)                          ;

v   =load(fullfile(PATH,strcat('data','.csv')))                   ;

surf(v)
title('Numerical')
colormap default
shading  interp
axis     equal
view(2)
set(gca,'xticklabel',[]   ,...
        'yticklabel',[]   ,...
        'xtick'     ,[]   ,...
        'ytick'     ,[]       )

saveas(gca,fullfile(PATH,strcat('data','.png')),'png')

end
